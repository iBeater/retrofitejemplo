package com.example.amartineza.ejercicioretrofit;

/**
 * Created by amartineza on 3/23/2018.
 */

public class CitiesModel {
    private Integer Id;
    private String Nombre;
    private String Latitud;
    private String Longitud;
    private String Estado;
    private Integer IdEstado;
    private String Pais;
    private Integer IdPais;
    private String Uris;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public String getNombre() {
        return Nombre;
    }

    public void setNombre(String nombre) {
        Nombre = nombre;
    }

    public String getLatitud() {
        return Latitud;
    }

    public void setLatitud(String latitud) {
        Latitud = latitud;
    }

    public String getLongitud() {
        return Longitud;
    }

    public void setLongitud(String longitud) {
        Longitud = longitud;
    }

    public String getEstado() {
        return Estado;
    }

    public void setEstado(String estado) {
        Estado = estado;
    }

    public Integer getIdEstado() {
        return IdEstado;
    }

    public void setIdEstado(Integer idEstado) {
        IdEstado = idEstado;
    }

    public String getPais() {
        return Pais;
    }

    public void setPais(String pais) {
        Pais = pais;
    }

    public Integer getIdPais() {
        return IdPais;
    }

    public void setIdPais(Integer idPais) {
        IdPais = idPais;
    }

    public String getUris() {
        return Uris;
    }

    public void setUris(String uris) {
        Uris = uris;
    }

}