package com.example.amartineza.ejercicioretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    Button loadButton;
    TextView logText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        loadButton = (Button) findViewById(R.id.loadButton);
        logText = (TextView) findViewById(R.id.logText);

        loadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getCallRetrofitService();
            }
        });
    }

    private void callRetrofitService() {
        String ENDPOINT = getString(R.string.host_api_stage);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();

        ModelRequest modelRequest = new ModelRequest();
        modelRequest.setCardNumber("1303030317482433");
        modelRequest.setCountryCode("");
        modelRequest.setTransactionInclue(false);

        SporaServices sporaServices = retrofit.create(SporaServices.class);

        sporaServices.getMembersLoyalty(modelRequest).enqueue(new Callback<ModelResponse>() {
            @Override
            public void onResponse(Call<ModelResponse> call, Response<ModelResponse> response) {
                if (response.isSuccessful()) {
                    logText.setText(response.toString());
                } else {
                    logText.setText(response.toString());
                }
            }

            @Override
            public void onFailure(Call<ModelResponse> call, Throwable t) {
                Log.e("", "onFailure");
            }
        });
    }

    private void getCallRetrofitService() {
        String ENDPOINT = getString(R.string.host_api_stage2);
        Retrofit retrofit = new Retrofit.Builder().baseUrl(ENDPOINT).addConverterFactory(GsonConverterFactory.create()).build();

        SporaServices sporaServices = retrofit.create(SporaServices.class);
        sporaServices.getCities().enqueue(new Callback<List<CitiesModel>>() {
            @Override
            public void onResponse(Call<List<CitiesModel>> call, Response<List<CitiesModel>> response) {
                logText.setText(response.toString());
            }

            @Override
            public void onFailure(Call<List<CitiesModel>> call, Throwable t) {
                Log.e("", "onFailure");
            }
        });
    }

}
