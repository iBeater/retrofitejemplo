package com.example.amartineza.ejercicioretrofit;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by amartineza on 3/23/2018.
 */

public class ModelResponse {
    private String name;
    private String email;
    @SerializedName("balance_list")
    private List<balance> balanceList;
    private level level;

    private String message;
    @SerializedName("status_code")
    private int statusCode;

    public class balance {
        private Double balance;
        private String key;
        private String name;
        private String message;

        public Double getBalance() {
            return balance;
        }

        public void setBalance(Double balance) {
            this.balance = balance;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public class level {
        @SerializedName("next_level")
        private String nextLevel;
        @SerializedName("advance_percent")
        private int advancePercent;
        private String key;
        private String name;
        private String message;

        public String getNextLevel() {
            return nextLevel;
        }

        public void setNextLevel(String nextLevel) {
            this.nextLevel = nextLevel;
        }

        public int getAdvancePercent() {
            return advancePercent;
        }

        public void setAdvancePercent(int advancePercent) {
            this.advancePercent = advancePercent;
        }

        public String getKey() {
            return key;
        }

        public void setKey(String key) {
            this.key = key;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    private List<transaction> transactions;

    public class transaction {
        private String cinema;
        private String message;
        private String date;
        private int points;

        public String getCinema() {
            return cinema;
        }

        public void setCinema(String cinema) {
            this.cinema = cinema;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public int getPoints() {
            return points;
        }

        public void setPoints(int points) {
            this.points = points;
        }
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }
}
