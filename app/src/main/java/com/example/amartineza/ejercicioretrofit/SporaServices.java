package com.example.amartineza.ejercicioretrofit;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;

/**
 * Created by amartineza on 3/23/2018.
 */

public interface SporaServices {

    @Headers({"api_key:cinepolis_test_android",
            "Authorization:bearer kUZiilXW6gCKlUT2NUdY3TCn_jvPapxzZDeYS4MJuCJEcnP7iWDNHwkCjagmG9wHGC8SF3UHwM95iGVcRi6wLQplVaTHjHkje_D2eblIMaQvV7nCsOpZYWbxxuUASjb-ubFXRLsvKWoSDBPGjwp5fdmIFYaV3-4qOp2cKZ1BERlLyGlL7wZSkW2Sl2KoOHoNXk7jJnS6UtN0yycXYyl0ZtPxoQPg74fCJKyQAIVBDOsa4o1LQiKPTPtSyAomP_JrmTvJJIy1k2b5vi_eZ_scmXpFB995MUHKbhZZJG-GTgN-_MLoEzcTFQoQDGMTJwNcV-I8xp0HPuT3byY_BfEBtZl9u3GjAbCHOyHJpqH8vzLEKn7oflnxLXggsP1F4wdfGS1Jnw"})
    @POST("v1/members/loyalty")
    Call<ModelResponse> getMembersLoyalty(@Body ModelRequest bodyMembersLoyalti);

    @GET("v1/cities")
    Call<List<CitiesModel>> getCities();
}
